package datastructure;

import cellular.CellState;


public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        if (rows <= 0 || columns <= 0 ) {
            throw new IndexOutOfBoundsException();
        } 

        grid = new CellState[rows][columns];
		this.rows = rows;
        this.columns = columns;
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                grid[row][column] = initialState;
            }
        }
        
	}


    @Override
    public int numRows() {
        return this.rows;
    } 

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || column < 0 || row >= this.rows || column >= this.columns) {
            throw new IndexOutOfBoundsException();
        } 
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(rows, columns, null);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                newGrid.set(row, column, this.get(row, column));
            }
        }
        return newGrid;
        

    }
    
}
