package cellular;

import datastructure.IGrid;

import java.util.Random;

import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState nextCell = CellState.DEAD;

		if (getCellState(row, col) == CellState.ALIVE) {
            nextCell = CellState.DYING;
        }
        else if (getCellState(row, col) == CellState.DYING) {
            nextCell = CellState.DEAD;
        }
        else if (aliveNeighbors == 2) {
            nextCell = CellState.ALIVE;
        }
        return nextCell;

        }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();

    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }


    private int countNeighbors(int row, int col, CellState state) {
		int count = 0;

		for (int x = row - 1; x <= row + 1; x++) {
			for (int y = col - 1; y <= col + 1; y++) {
				try {
					CellState temp = getCellState(x, y);
					if (x == row && y == col) {
						continue;
					}
					if (temp.equals(state)) {
						count++;
					}
				}
					catch (Exception e) {
						continue;
					}
			
				}
			}
			return count;
		}
    
}
